#include "uthread.h"
#include <stdio.h>

void g(void* data)
{
    printf("In g(), data=%lx\n", (unsigned long) data);
}

void f(void* data)
{
    for (int i = 0; i < 3; ++i) {
        printf("In f(), i=%d, data=%lx\n", i, (unsigned long) data);
        uthread_start(g, data);
        uthread_yield();
    }
}

int main()
{
    printf("In main()\n");   
    uthread_start(f, (void*) 0x555);
    while (!uthread_try_join()) {
        printf("In main()\n");
        uthread_yield();
    }
}

