#include "syscall.h"

int main()
{
    pid_t p = fork();
    if (!p) {
        exit(0);
    }
    int status;
    pid_t r = waitpid(p, &status, 0);
    if (r != p || (status & 0x7f) != 0)
        return 1;
    return 0;
}
